import { defineStore } from 'pinia';

import { ref } from 'vue';
import { Quasar } from 'quasar';

export const useMainStore = defineStore('main', () => {
  const data = ref('codfish');

  return {
    data,
  }
})