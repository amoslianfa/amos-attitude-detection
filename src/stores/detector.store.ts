import { useAsyncState } from '@vueuse/core';
import { defineStore, acceptHMRUpdate } from 'pinia';
import { useQuasar } from 'quasar';
import { ref } from 'vue';

import * as poseDetection from '@tensorflow-models/pose-detection';
import * as tf from '@tensorflow/tfjs-core';
import '@tensorflow/tfjs-backend-webgl';
import '@tensorflow/tfjs-backend-webgpu';

export const useDetectorStore = defineStore('detector', () => {
  const $q = useQuasar();

  const { state: detector } = useAsyncState(async () => {
    $q.loading.show({ message: '正在初始化 TensorFlow.js' });
    await tf.ready();

    $q.loading.show({ message: '正在下載 TensorFlow MoveNet 模型...' });
    const model = poseDetection.SupportedModels.MoveNet;
    const result = await poseDetection.createDetector(model);
    return result;
  }, undefined, {
    shallow: true,
    onError(error) {
      $q.notify({
        type: 'negative',
        message: `初始化 TensorFlow.js 發生錯誤 : ${error}`
      });
      console.error(error);
    },
    onSuccess() {
      $q.loading.hide();

      $q.notify({
        type: 'positive',
        message: ` TensorFlow.js 初始化完成`
      });
    }
  });

  return {
    detector,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useDetectorStore, import.meta.hot))
}